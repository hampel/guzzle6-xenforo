<?php

namespace Guzzle6\Test;

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;

class ClientTest extends AbstractTest
{
	public function run()
	{
		$urls = [
			['action' => 'GET', 'url' => 'http://httpbin.org/get'],
			['action' => 'GET', 'url' => 'http://httpbin.org/redirect/2'],
			['action' => 'GET', 'url' => 'http://httpbin.org/redirect-to?url=/get'],
			['action' => 'POST', 'url' => 'http://httpbin.org/post'],
			['action' => 'PUT', 'url' => 'http://httpbin.org/put'],
			['action' => 'DELETE', 'url' => 'http://httpbin.org/delete'],
		];

		$client = $this->app->http()->client();


		foreach ($urls as $url)
		{
			try
			{
				$request = new Request($url['action'], $url['url']);
				$response = $client->send($request);
				if (!$response)
				{
					$this->errorMessage(\XF::phrase('guzzle6_url_x_could_not_be_fetched_or_is_not_valid_error_y', ['url' => $url['url'], 'error' => '']));
					return false;
				}

				$this->successMessage(\XF::phrase('guzzle6_url_x_was_fetched_successfully', ['url' => $url['url']]));
			}
			catch (RequestException $e)
			{
				$this->errorMessage(\XF::phrase('guzzle6_url_x_could_not_be_fetched_or_is_not_valid_error_y', ['url' => $url['url'], 'error' => $e->getMessage()]));
				return false;
			}
		}

		return true;
	}
}