<?php namespace Guzzle6\Test;

class CaptchaTest extends AbstractTest
{
	public function run()
	{
		if (!empty(\XF::options()->captcha) && !$this->controller->captchaIsValid())
		{
			$this->errorMessage(\XF::phrase('did_not_complete_the_captcha_verification_properly'));
			return false;
		}

		return true;
	}
}