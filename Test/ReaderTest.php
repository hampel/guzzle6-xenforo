<?php

namespace Guzzle6\Test;

use GuzzleHttp\Exception\RequestException;

class ReaderTest extends AbstractTest
{
	public function run()
	{
		$urls = [
			'http://httpbin.org/get',
			'http://httpbin.org/redirect/2',
			'http://httpbin.org/redirect-to?url=/get',
			'http://httpbin.org/stream/100',
		];

		$reader = $this->app->http()->reader();

		foreach ($urls as $url)
		{
			try
			{
				$response = $reader->getUntrusted($url, [], null, [], $error);
				if (!$response)
				{
					$this->errorMessage(\XF::phrase('guzzle6_url_x_could_not_be_fetched_or_is_not_valid_error_y', ['url' => $url, 'error' => $error]));
					return false;
				}

				$this->successMessage(\XF::phrase('guzzle6_url_x_was_fetched_successfully', ['url' => $url]));
			}
			catch (RequestException $e)
			{
				$this->errorMessage(\XF::phrase('guzzle6_url_x_could_not_be_fetched_or_is_not_valid_error_y', ['url' => $url, 'error' => $e->getMessage()]));
				return false;
			}
		}

		$url = 'http://127.0.0.1/';

		try
		{
			$response = $reader->getUntrusted($url, [], null, [], $error);
			if (!$response && $error == 'The URL is not requestable (local: 127.0.0.1)')
			{
				$this->successMessage(\XF::phrase('guzzle6_url_x_could_not_be_fetched_or_is_not_valid_error_y_as_expected', ['url' => $url, 'error' => $error]));
			}
			else
			{
				$this->errorMessage(\XF::phrase('guzzle6_url_x_unexpected_result_error_y', ['url' => $url, 'error' => $error]));
				return false;
			}
		}
		catch (RequestException $e)
		{
			$this->errorMessage(\XF::phrase('guzzle6_url_x_unexpected_result_error_y', ['url' => $url, 'error' => $e->getMessage()]));
			return false;
		}

		return true;
	}
}