<?php namespace Guzzle6\Test;

class EmbedDataTitleTest extends AbstractTest
{
	public function run()
	{
		$xfmg = $this->app->get('addon.manager')->getById('XFMG');
		if (is_null($xfmg))
		{
			$this->errorMessage('XFMG is not installed');
			return false;
		}
		elseif (!$xfmg->isActive())
		{
			$this->errorMessage('XFMG is not active');
			return false;
		}

		/** @var \XFMG\Repository\Media $mediaRepo */
		$mediaRepo = $this->app->repository('XFMG:Media');

		$embedDataHandler = $mediaRepo->createEmbedDataHandler('');
		$titleData = $embedDataHandler->getTitleAndDescription('https://www.youtube.com/watch?v=weqq3U-U8VY', '', '');

		if (is_array($titleData) && isset($titleData['title']) && !empty($titleData['title'])) return true;

		$this->errorMessage('Did not find title');
		return false;
	}
}