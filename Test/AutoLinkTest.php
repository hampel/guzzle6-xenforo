<?php

namespace Guzzle6\Test;

class AutoLinkTest extends AbstractTest
{
	public function run()
	{
		$url = 'http://httpbin.org/';
		$expected = '[URL="http://httpbin.org/"]httpbin(1): HTTP Client Testing Service[/URL]';

		$bbcode = $this->app->bbCode();
		$processor = $bbcode->processor();
		$processor->addProcessorAction('autolink', $bbcode->processorAction('autolink'));

		$rendered = $processor->render($url, $bbcode->parser(), $bbcode->rules('editor'));
		if ($rendered == $expected)
		{
			return true;
		}
		else
		{
			$group = $this->app->finder('XF:OptionGroup')->whereId('messageOptions')->fetchOne();

			$this->errorMessage(\XF::phrase('guzzle6_error_rendering_title_for_url_expected_received', [
				'url' => $url,
				'expected' => $expected,
				'rendered' => $rendered,
				'optionurl' => $this->controller->buildLink('full:options/groups', $group) . "#urlToPageTitle",
			]));
			return false;
		}
	}
}