<?php

namespace Guzzle6\Test;

use XF\Entity\User;
use XF\Mvc\Entity\Structure;

class AkismetTest extends AbstractTest
{
	public function run()
	{
		if (empty($this->app->options()->akismetKey))
		{
			$group = $this->app->finder('XF:OptionGroup')->whereId('spam')->fetchOne();

			$this->errorMessage(\XF::phrase('guzzle6_error_akismet_key_not_set', [
				'optionurl' => $this->controller->buildLink('full:options/groups', $group) . "#akismetKey"
			]));
			return false;
		}

		$user = new User($this->app->em(), User::getStructure(new Structure()));
		$user->username = 'viagra-test-123'; // trigger spam

		$checker = $this->app->spam()->contentChecker();
		$checker->check($user, '', ['is_test' => true]);

		$decision = $checker->getFinalDecision();

		if ($decision == 'moderated')
		{
			$this->successMessage(\XF::phrase('guzzle6_akismet_spam_was_successfully_detected'));
		}
		else
		{
			$this->errorMessage(\XF::phrase('guzzle6_akismet_expected_spam'));
			return false;
		}

		$user->username = 'administrator'; // trigger ham

		$checker->check($user, '', ['is_test' => true]);

		$decision = $checker->getFinalDecision();

		if ($decision == 'allowed')
		{
			$this->successMessage(\XF::phrase('guzzle6_akismet_ham_was_successfully_detected'));
		}
		else
		{
			$this->errorMessage(\XF::phrase('guzzle6_akismet_expected_ham'));
			return false;
		}

		return true;
	}
}