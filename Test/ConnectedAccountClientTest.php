<?php namespace Guzzle6\Test;

use OAuth\Common\Http\Uri\Uri;
use GuzzleHttp\Exception\RequestException;

class ConnectedAccountClientTest extends AbstractTest
{
	public function run()
	{
		$urls = [
			['action' => 'GET', 'url' => 'http://httpbin.org/get'],
			['action' => 'GET', 'url' => 'http://httpbin.org/redirect/2'],
			['action' => 'GET', 'url' => 'http://httpbin.org/redirect-to?url=/get'],
			['action' => 'POST', 'url' => 'http://httpbin.org/post', 'body' => 'foo'],
			['action' => 'POST', 'url' => 'http://httpbin.org/post', 'body' => ['foo' => 'bar']],
			['action' => 'POST', 'url' => 'http://httpbin.org/post', 'body' => []],
			['action' => 'PUT', 'url' => 'http://httpbin.org/put', 'body' => ''],
			['action' => 'PUT', 'url' => 'http://httpbin.org/put', 'body' => ['foo' => 'bar']],
			['action' => 'DELETE', 'url' => 'http://httpbin.org/delete'],
		];

		$client = $this->app->oAuth()->client();

		foreach ($urls as $url)
		{
			try
			{
				$response = $client->retrieveResponse(new Uri($url['url']), isset($url['body']) ? $url['body'] : null, [], $url['action']);
				if (!$response)
				{
					$this->errorMessage(\XF::phrase('guzzle6_url_x_could_not_be_fetched_or_is_not_valid_error_y', ['url' => $url['url'], 'error' => '']));
					return false;
				}

				$this->successMessage(\XF::phrase('guzzle6_url_x_was_fetched_successfully', ['url' => $url['url']]));
			}
			catch (RequestException $e)
			{
				$this->errorMessage(\XF::phrase('guzzle6_url_x_could_not_be_fetched_or_is_not_valid_error_y', ['url' => $url['url'], 'error' => $e->getMessage()]));
				return false;
			}
		}

		return true;
	}
}
