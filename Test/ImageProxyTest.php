<?php

namespace Guzzle6\Test;

class ImageProxyTest extends AbstractTest
{
	public function run()
	{
		$url = 'http://httpbin.org/image/png';

		/** @var \XF\Service\ImageProxy $proxyService */
		$proxyService = $this->controller->service('XF:ImageProxy');
		$results = $proxyService->testImageFetch($url);
		if ($results)
		{
			if ($results['valid']) return true;
			else
			{
				$this->errorMessage($results['error']);
				return false;
			}
		}

		return false;
	}
}