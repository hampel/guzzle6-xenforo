<?php

namespace Guzzle6\Test;

use XF\Entity\User;
use XF\Mvc\Entity\Structure;

class StopForumSpamTest extends AbstractTest
{
	public function run()
	{
		$user = new User($this->app->em(), User::getStructure(new Structure()));
		$user->username = 'xrumer';
		$user->email = 'xrumertest@this.baddomain.com';

		$checker = $this->app->spam()->userChecker();

		$class = $this->app->extendClass('XF\Spam\Checker\StopForumSpam');
		$sfs = new $class($checker, $this->app);

		$sfs->check($user);
		$decision = $checker->getFinalDecision();

		if ($decision == 'moderated')
		{
			$this->successMessage(\XF::phrase('guzzle6_stopforumspam_spam_was_successfully_detected'));
		}
		else
		{
			$this->errorMessage(\XF::phrase('guzzle6_stopforumspam_expected_spam'));
			return false;
		}

		return true;
	}
}