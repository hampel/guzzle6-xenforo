<?php namespace Guzzle6\Test;

class EmbedDataThumbnailTest extends AbstractTest
{
	public function run()
	{
		$xfmg = $this->app->get('addon.manager')->getById('XFMG');
		if (is_null($xfmg))
		{
			$this->errorMessage('XFMG is not installed');
			return false;
		}
		elseif (!$xfmg->isActive())
		{
			$this->errorMessage('XFMG is not active');
			return false;
		}

		/** @var \XFMG\Repository\Media $mediaRepo */
		$mediaRepo = $this->app->repository('XFMG:Media');

		$embedDataHandler = $mediaRepo->createEmbedDataHandler('');
		$tempFile = $embedDataHandler->getTempThumbnailPath('https://www.youtube.com/watch?v=weqq3U-U8VY', '', '');

		if (file_exists($tempFile)) return true;

		$this->errorMessage('Did not find temp file');
		return false;
	}
}