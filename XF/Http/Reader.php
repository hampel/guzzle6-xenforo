<?php

namespace Guzzle6\XF\Http;

use GuzzleHttp\Psr7\Uri;
use GuzzleHttp\ClientInterface;
use Psr\Http\Message\ResponseInterface;

class Reader extends \XF\Http\Reader
{
	public function __construct(ClientInterface $clientTrusted, ClientInterface $clientUntrusted)
	{
		$this->clientTrusted = $clientTrusted;
		$this->clientUntrusted = $clientUntrusted;
	}

	/**
	* @param       $url
	* @param array $limits
	* @param null  $saveTo
	* @param array $options
	* @param null  $error
	* @param callable[] $listeners
	*
	* @return \GuzzleHttp\Message\ResponseInterface|null
	*/
	public function getUntrusted($url, array $limits = [], $saveTo = null, array $options = [], &$error = null, array $listeners = [])
	{
		$options['allow_redirects'] = false;

		$requests = 0;

		do
		{
			$continue = false;
			$requests++;

			$url = preg_replace('/#.*$/', '', $url);
			if (!$this->isRequestableUntrustedUrl($url, $untrustedError))
			{
				$error = "The URL is not requestable ($untrustedError)";
				return null;
			}

			$response = $this->_get($this->clientUntrusted, $url, $limits, $saveTo, $options, $error, $listeners);

			if (
				$response instanceof ResponseInterface
				&& $response->getStatusCode() >= 300
				&& $response->getStatusCode() < 400
				&& ($location = $response->getHeader('Location'))
			)
			{
				$location = new Uri(implode(', ', $location));

				if (!Uri::isAbsolute($location)) {
					$originalUrl = \GuzzleHttp\Url::fromString($url);

					$originalUrl->getQuery()->clear();

					$location = $originalUrl->combine($location);
				}
				$location = strval($location);

				if ($location != $url)
				{
					$url = $location;
					$continue = true;
				}
			}
		}
		while ($continue && $requests < 5);

		return $response;
	}


	/**
	 * @param \GuzzleHttp\Client $client
	 * @param string $url
	 * @param array $limits
	 * @param null|string $saveTo
	 * @param array $options
	 * @param mixed $error
	 * @param callable[] $listeners
	 *
	 * @return \GuzzleHttp\Message\ResponseInterface|null
	 */
	protected function _get(
		\GuzzleHttp\Client $client,
		$url, array $limits = [], $saveTo = null, array $options = [], &$error = null, array $listeners = []
	)
	{
		$limits = array_merge([
			'time' => -1,
			'bytes' => -1
		], $limits);
		$maxTime = intval($limits['time']);
		$maxSize = intval($limits['bytes']);

		$options = array_merge([
			'decode_content' => 'identity',
			'timeout' => $maxTime > -1 ? $maxTime + 1 : 30,
			'connect_timeout' => 3,
			'exceptions' => false
		], $options);

		if (!$saveTo)
		{
			$saveTo = 'php://temp';
		}

		if (is_string($saveTo))
		{
			$closeOnError = true;
			$saveTo = fopen($saveTo, 'w+');
		}
		else
		{
			$closeOnError = false;
		}

		$saveTo = \GuzzleHttp\Psr7\stream_for($saveTo);
		$saveTo = new Stream($saveTo, $maxSize, $maxTime);

		$options['sink'] = $saveTo;

		$this->initListeners($client, $listeners);

		try
		{
			$response = $client->get($url, $options);
		}
		catch (\GuzzleHttp\Exception\RequestException $e)
		{
			if ($saveTo->hasError($errorCode))
			{
				$error = $this->getErrorMessage($errorCode);
			}
			else
			{
				$error = $e->getMessage();
			}

			if ($closeOnError)
			{
				$saveTo->close();
			}

			return null;
		}

		if ($saveTo->hasError($errorCode))
		{
			$error = $this->getErrorMessage($errorCode);

			if ($closeOnError)
			{
				$saveTo->close();
			}

			return null;
		}

		return $response;
	}

	/**
	 * This is called with every _get(), and causes previously attached listener callbacks to be detached
	 * and any new listeners to be attached. The primary use for this would be a 'progress' listener,
	 * operating something like ['progress' => function (\GuzzleHttp\Event\ProgressEvent $e) { ... }]
	 *
	 * @param \GuzzleHttp\Client $client
	 * @param callable[] $listeners
	 *
	 * @return bool
	 */
	protected function initListeners(\GuzzleHttp\Client $client, array $listeners)
	{
		/*
		 * Guzzle6 uses Middleware attached to the client rather than listeners, so this function does nothing
		 *
		 * If you need to add middleware to your client, hook into the http_client_config_guzzle6 XenForo event listener
		 * and attach your middleware
		 *
		 * Refer to the UPGRADING document here:
		 * https://github.com/guzzle/guzzle/blob/master/UPGRADING.md#migrating-to-middleware for details about migrating
		 * from listeners to middleware
		 *
		 * Refer to the Handlers and Middleware documentation for Guzzle 6 for more information
		 * http://docs.guzzlephp.org/en/stable/handlers-and-middleware.html
		 */

		return true;
	}

	public function getCharset($contentType)
	{
		// Guzzle6: getHeader('Content-type') now returns an array
		$contentTypeString = array_shift($contentType);
		return parent::getCharset($contentTypeString);
	}
}