<?php namespace Guzzle6\XF\Captcha;

use XF\Template\Templater;

class TextCaptcha extends XFCP_TextCaptcha
{
	public function renderInternal(Templater $templater)
	{
		$extraKeys = $this->app->options()->extraCaptchaKeys;
		$apiKey = !empty($extraKeys['textCaptchaApiKey']) ? $extraKeys['textCaptchaApiKey'] : null;

		try
		{
			$client = $this->app->http()->client();
			// Guzzle6 does not support json method on response objects
			$response = \GuzzleHttp\json_decode($client->get("http://api.textcaptcha.com/$apiKey.json")->getBody()->getContents(), true);
		}
		catch (\GuzzleHttp\Exception\RequestException $e)
		{
			// this is an exception with the underlying request, so let it go through
			\XF::logException($e, false, 'Error fetching textCAPTCHA: ');
			$response = null;
		}
		if (!$response)
		{
			$response = [
				'q' => '',
				'a' => ['failed']
			];
		}
		$response['hash'] = md5($response['q'] . uniqid() . memory_get_usage());

		$captchaLog = $this->app->em()->create('XF:CaptchaLog');
		$captchaLog->bulkSet([
			'hash' => $response['hash'],
			'captcha_type' => 'TextCaptcha',
			'captcha_data' => implode(',', $response['a']),
			'captcha_date' => time()
		]);
		$captchaLog->save();

		return $templater->renderTemplate('public:captcha_textcaptcha', [
			'question' => $response
		]);
	}
}
