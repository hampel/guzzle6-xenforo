<?php namespace Guzzle6\XF\Captcha;

class ReCaptcha extends XFCP_ReCaptcha
{
	public function isValid()
	{
		if (!$this->siteKey || !$this->secretKey)
		{
			return true; // if not configured, always pass
		}

		$request = $this->app->request();

		$captchaResponse = $request->filter('g-recaptcha-response', 'str');
		if (!$captchaResponse)
		{
			return false;
		}

		try
		{
			$client = $this->app->http()->client();

			// Guzzle6 deprecates passing the body request option as an array to send a POST request
			// also, there is no longer a ->json() method on the response object thanks to PSR7
			$response = \GuzzleHttp\json_decode($client->post('https://www.google.com/recaptcha/api/siteverify',
				['form_params' => [
					'secret' => $this->secretKey,
					'response' => $captchaResponse,
					'remoteip' => $request->getIp()
				]
			])->getBody()->getContents(), true);

			if (isset($response['success']) && isset($response['hostname']) && $response['hostname'] == $request->getHost())
			{
				return $response['success'];
			}

			return false;
		}
		catch(\GuzzleHttp\Exception\RequestException $e)
		{
			// this is an exception with the underlying request, so let it go through
			\XF::logException($e, false, 'ReCAPTCHA connection error: ');
			return true;
		}
	}
}
