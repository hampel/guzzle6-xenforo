<?php namespace Guzzle6\XF\ConnectedAccount\Http;

use OAuth\Common\Http\Uri\UriInterface;
use XF\ConnectedAccount\Http\HttpResponseException;

class Client extends XFCP_Client
{
	public function retrieveResponse(UriInterface $endpoint, $requestBody, array $extraHeaders = [], $method = 'POST')
	{
		$method = strtoupper($method);

		if ($method === 'GET' && !empty($requestBody))
		{
			throw new \InvalidArgumentException('No body expected for "GET" request.');
		}

		// Guzzle6 will set this for us when we use the form_params paramater
//		if (!isset($extraHeaders['Content-Type']) && $method === 'POST' && is_array($requestBody))
//		{
//			$extraHeaders['Content-Type'] = 'application/x-www-form-urlencoded';
//		}

		$extraHeaders['Host'] = $endpoint->getHost();
		$extraHeaders['Connection'] = 'close';
		$extraHeaders['User-Agent'] = $this->userAgent;

		$client = \XF::app()->http()->client();

		// Guzzle6: Sending application/x-www-form-urlencoded POST requests requires that you specify the
		// POST fields as an array in the form_params request options.
		$requestBodyField = 'body';
		if ($method === 'POST' || $method === 'PUT')
		{
			if (is_array($requestBody))
			{
				$requestBodyField = 'form_params';
			}
		}
		$extraHeaders['Content-length'] = ($requestBody && is_string($requestBody)) ? strlen($requestBody) : 0;

		$response = $client->request($method, $endpoint->getAbsoluteUri(), [
			'allow_redirects' => [
				'max' => $this->maxRedirects
			],
			$requestBodyField => $requestBody,
			'headers' => $extraHeaders,
			'timeout' => $this->timeout,
			'exceptions' => false
		]);

		$body = $response->getBody();
		$content = $body ? $body->getContents() : '';

		$code = $response->getStatusCode();
		if ($code >= 400)
		{
			$exception = new HttpResponseException("Failed to request resource. HTTP Code: $code", $code);
			$exception->setResponseContent($content);
			throw $exception;
		}

		if (!$body)
		{
			$exception = new HttpResponseException("Failed to request resource. No body.", $code);
			$exception->setResponseContent($content);
			throw $exception;
		}

		return $content;
	}
}
