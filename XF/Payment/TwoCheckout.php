<?php namespace Guzzle6\XF\Payment;

use XF\Mvc\Controller;
use XF\Purchasable\Purchase;
use XF\Payment\CallbackState;
use XF\Entity\PaymentProfile;
use XF\Entity\PurchaseRequest;

class TwoCheckout extends XFCP_TwoCheckout
{
	public function processPayment(Controller $controller, PurchaseRequest $purchaseRequest, PaymentProfile $paymentProfile, Purchase $purchase)
	{
		$input = $controller->filter([
			'sid' => 'str',
			'key' => 'str',
			'order_number' => 'str',
			'credit_card_processed' => 'str',
			'merchant_order_id' => 'str',
			'demo' => 'str'
		]);
		$options = $paymentProfile->options;

		$md5Hash = strtoupper(md5(
			$options['secret_word'] .
			$options['account_number'] .
			($input['demo'] === 'Y' ? 1 : $input['order_number']) .
			$purchaseRequest->cost_amount
		));

		if ($md5Hash !== $input['key'])
		{
			throw $controller->exception($controller->error('Could not verify 2Checkout hash.'));
		}

		if ($input['credit_card_processed'] === 'Y')
		{
			$state = new CallbackState();
			$state->transactionId = $input['key'];
			$state->paymentResult = CallbackState::PAYMENT_RECEIVED;
			$state->purchaseRequest = $purchaseRequest;
			$state->paymentProfile = $paymentProfile;

			// If this is a recurring purchase, and an API username/password have been set in the profile
			// then we need to grab the line item IDs and store them as the subscriber ID.
			// At such a time that the user may wish to cancel their subscription, we can
			// cancel the recurring payment for each line item.
			if ($purchase->recurring && $options['api_username'] && $options['api_password'])
			{
				try
				{
					$client = \XF::app()->http()->client();

					$saleResponse = $client->get($this->getApiEndpoint() . '/sales/detail_sale', [
						'headers' => ['Accept' => 'application/json'],
						'auth' => [$options['api_username'], $options['api_password']],
						'query' => ['sale_id' => $input['order_number']]
					]);
					// Guzzle6 does not support json method on response objects
					$sale = \GuzzleHttp\json_decode($saleResponse->getBody()->getContents(), true);

					$lineItemIds = [];
					if (isset($sale['sale']['invoices']) && is_array($sale['sale']['invoices']))
					{
						foreach ($sale['sale']['invoices'] AS $invoice)
						{
							foreach ($invoice['lineitems'] AS $lineItem)
							{
								$lineItemIds[] = $lineItem['lineitem_id'];
							}
						}
					}
					$purchaseRequest->provider_metadata = \GuzzleHttp\json_encode($lineItemIds);
					$purchaseRequest->save(false);
				}
				catch (\GuzzleHttp\Exception\RequestException $e) {}
			}

			$this->completeTransaction($state);

			$this->log($state);
		}

		return $controller->redirect($purchase->returnUrl, '');
	}

	public function processCancellation(Controller $controller, PurchaseRequest $purchaseRequest, PaymentProfile $paymentProfile)
	{
		$lineItemIds = \GuzzleHttp\json_decode($purchaseRequest->provider_metadata, true);

		if (!$lineItemIds)
		{
			return $controller->error('This purchase cannot be cancelled.');
		}

		$hasError = false;
		$options = $paymentProfile->options;

		$client = \XF::app()->http()->client();

		try
		{
			foreach ($lineItemIds AS $lineItemId)
			{
				// Guzzle6 deprecates passing the body request option as an array to send a POST request
				$cancelResponse = $client->post($this->getApiEndpoint() . '/sales/stop_lineitem_recurring', [
					'headers' => ['Accept' => 'application/json'],
					'auth' => [$options['api_username'], $options['api_password']],
					'form_params' => [
						'vendor_id' => $options['account_number'],
						'lineitem_id' => $lineItemId
					]
				]);
				// Guzzle6 does not support json method on response objects
				$cancellation = \GuzzleHttp\json_decode($cancelResponse->getBody()->getContents(), true);
				if (!$cancellation || $cancellation['response_code'] != 'OK')
				{
					$hasError = true;
				}
			}
		}
		catch (\GuzzleHttp\Exception\RequestException $e) {}

		if ($hasError)
		{
			throw $controller->exception($controller->error('This subscription cannot be cancelled. It may already be cancelled.'));
		}

		return $controller->redirect($controller->getDynamicRedirect(), '2Checkout subscription cancelled successfully');
	}
}
