<?php namespace Guzzle6\XF\Payment;

use XF\Payment\CallbackState;

class PayPal extends XFCP_PayPal
{
	public function validateCallback(CallbackState $state)
	{
		try
		{
			// Guzzle6 deprecates passing the body request option as an array to send a POST request
			$params = ['form_params' => $state->_POST + ['cmd' => '_notify-validate']];
			$client = \XF::app()->http()->client();
			if ($state->testMode)
			{
				$url = 'https://ipnpb.sandbox.paypal.com/cgi-bin/webscr';
			}
			else
			{
				$url = 'https://ipnpb.paypal.com/cgi-bin/webscr';
			}
			$response = $client->post($url, $params);
			// Guzzle6 streams - need to getContents()
			if (!$response || $response->getBody()->getContents() != 'VERIFIED' || $response->getStatusCode() != 200)
			{
				$host = \XF\Util\Ip::getHost($state->ip);
				if (preg_match('#(^|\.)paypal.com$#i', $host))
				{
					$state->logType = 'error';
					$state->logMessage = 'Request not validated';
				}
				else
				{
					$state->logType = false;
					$state->logMessage = 'Request not validated (from unknown source)';
				}
				return false;
			}
		}
		catch (\GuzzleHttp\Exception\RequestException $e)
		{
			$state->logType = 'error';
			$state->logMessage = 'Connection to PayPal failed: ' . $e->getMessage();
			return false;
		}

		return true;
	}
}
