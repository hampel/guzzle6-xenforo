<?php namespace Guzzle6\XF\Service\AddOn;

class JsMinifier extends XFCP_JsMinifier
{
	protected function request($getErrors = false)
	{
		$client = $this->client;
		$options = $this->options;

		if ($getErrors)
		{
			$options['output_info'] = 'errors';
		}

		try
		{
			// Guzzle6 deprecates passing the body request option as an array to send a POST request
			$response = $client->post('https://closure-compiler.appspot.com/compile', [
				'form_params' => $options
			]);
			// Guzzle6 does not support json method on response objects
			return \GuzzleHttp\json_decode($response->getBody()->getContents(), true);
		}
		catch (\GuzzleHttp\Exception\RequestException $e)
		{
			return null;
		}
	}
}
