<?php

namespace Guzzle6\XF\Service;

class ImageProxy extends XFCP_ImageProxy
{
	protected function fetchImageDataFromUrl($url)
	{
		$url = $this->proxyRepo->cleanUrlForFetch($url);
		if (!preg_match('#^https?://#i', $url))
		{
			throw new \InvalidArgumentException("URL must be http or https");
		}

		$urlParts = @parse_url($url);

		$validImage = false;
		$fileName = !empty($urlParts['path']) ? basename($urlParts['path']) : null;
		$mimeType = null;
		$error = null;
		$streamFile = \XF\Util\File::getTempDir() . '/' . strtr(md5($url) . '-' . uniqid(), '/\\.', '---') . '.temp';
		$imageProxyMaxSize = $this->app->options()->imageProxyMaxSize * 1024;

		try
		{
			$options = [
				'headers' => [
					'Accept' => 'image/*,*/*;q=0.8'
				]
			];
			$limits = [
				'time' => 8,
				'bytes' => $imageProxyMaxSize ?: -1
			];
			$response = $this->app->http()->reader()->getUntrusted($url, $limits, $streamFile, $options, $error);
		}
		catch (\Exception $e)
		{
			$response = null;
			$error = $e->getMessage();
		}

		if ($response)
		{
			$response->getBody()->close();

			if ($response->getStatusCode() == 200)
			{
				$disposition = $response->getHeader('Content-Disposition');

				// Guzzle6: getHeader('Content-Disposition') returns an array
				if (!empty($disposition) && preg_match('/filename=(\'|"|)(.+)\\1/siU', $disposition[0], $match))
				{
					$fileName = $match[2];
				}
				if (!$fileName)
				{
					$fileName = 'image';
				}

				$imageInfo = filesize($streamFile) ? @getimagesize($streamFile) : false;
				if ($imageInfo)
				{
					$imageType = $imageInfo[2];

					$extension = \XF\Util\File::getFileExtension($fileName);
					$extensionMap = [
						IMAGETYPE_GIF => ['gif'],
						IMAGETYPE_JPEG => ['jpg', 'jpeg', 'jpe'],
						IMAGETYPE_PNG => ['png']
					];
					if (isset($extensionMap[$imageType]))
					{
						$mimeType = $imageInfo['mime'];

						$validExtensions = $extensionMap[$imageType];
						if (!in_array($extension, $validExtensions))
						{
							$extensionStart = strrpos($fileName, '.');
							$fileName = (
								$extensionStart
									? substr($fileName, 0, $extensionStart)
									: $fileName
								) . '.' . $validExtensions[0];
						}

						$validImage = true;
					}
					else
					{
						$error = \XF::phraseDeferred('image_is_invalid_type');
					}
				}
				else
				{
					$error = \XF::phraseDeferred('file_not_an_image');
				}
			}
			else
			{
				$error = \XF::phraseDeferred('received_unexpected_response_code_x_message_y', [
					'code' => $response->getStatusCode(),
					'message' => $response->getReasonPhrase()
				]);
			}
		}

		if (!$validImage)
		{
			@unlink($streamFile);
		}

		return [
			'valid' => $validImage,
			'error' => $error,
			'dataFile' => $validImage ? $streamFile : null,
			'fileName' => $fileName,
			'mimeType' => $mimeType
		];
	}
}