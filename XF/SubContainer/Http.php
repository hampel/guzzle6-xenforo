<?php

namespace Guzzle6\XF\SubContainer;

class Http extends \XF\SubContainer\Http
{
	public function initialize()
	{
		$container = $this->container;

		$container['client'] = function($c)
		{
			return $this->createClient();
		};
		$container['clientUntrusted'] = function($c)
		{
			// for Guzzle 6, we need to set options before we create the client
			$options = ['allow_redirects' => false];

			$config = $this->app->config();
			if ($config['http']['proxy'])
			{
				$options['proxy'] = $config['http']['proxy'];
			}

			// fire a custom event so that addon developers can set custom options as required
			$this->app->fire('http_client_config_untrusted_guzzle6', [&$options]);

			$client = $this->createClient($options);

			// fire the core event - but note that custom options cannot be set here
			$this->app->fire('http_client_config_untrusted', [&$client]);

			return $client;
		};

		$container['reader'] = function($c)
		{
			return new \Guzzle6\XF\Http\Reader($c['client'], $c['clientUntrusted']);
		};
	}

	// need our own function because signature is different
	protected function generateDefaultClientConfig(&$options)
	{
		$config = $this->app->config();
		if ($config['http']['sslVerify'] === null)
		{
			$bundleFileName = 'ca-bundle.crt';

			if (extension_loaded('curl')) // this should always be true...
			{
				$version = curl_version();
				if (preg_match('#openssl/(0|1\.0\.[01])#i', $version['ssl_version']))
				{
					// For OpenSSL < 1.0.2, we need to use a bundle that includes the Equifax cert as it will
					// always check if it knows the last certificate in the path. Google cross signs their certificates
					// with a known cert and the now-untrusted Equifax cert. See this for more details:
					// https://serverfault.com/questions/841036/openssl-unable-to-get-local-issuer-certificate-for-accounts-google-com
					$bundleFileName = 'ca-bundle-legacy-openssl.crt';
				}
			}

			$verify = \XF::getSourceDirectory() . "/XF/Http/" . $bundleFileName;
		}
		else
		{
			$verify = $config['http']['sslVerify'];
		}
		$options['verify'] = $verify;

		$xfoptions = $this->parent['options'];
		$options['headers'] = ['User-Agent' => 'XenForo/2.x (' . $xfoptions->boardUrl . ')'];

		// fire a custom event so addons can make config changes
		$this->app->fire('http_client_config_guzzle6', [&$options]);
	}

	public function createClient(array $options = [])
	{
		// for Guzzle 6, we need to set options before we create the client
		$this->generateDefaultClientConfig($options);
		$client = new \GuzzleHttp\Client($options);

		// fire the core event but note that config changes cannot be made at this point
		$this->app->fire('http_client_config', [&$client]);

		return $client;
	}
}