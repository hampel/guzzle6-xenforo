<?php namespace Guzzle6\XF\Spam\Checker;

use GuzzleHttp\Psr7\Request;

class Akismet extends XFCP_Akismet
{
	protected function _submitSpam($params)
	{
		if (!empty($params['akismetIsSpam']))
		{
			return; // Akismet already told us this is spam, so do not submit again.
		}

		if (!empty($params['akismet']))
		{
			try
			{
				$params = [
					'headers' => [
						'User-Agent' => $this->getUserAgent()
					],
					'form_params' => $params['akismet'],
				];

				$response = $this->getHttpClient()->post('http://' . $this->getApiKey() . '.rest.akismet.com/1.1/submit-spam', $params);
				$response = trim($response->getBody()->getContents());

				if ($response == 'invalid')
				{
					throw new \InvalidArgumentException("Invalid API key");
				}
			}
			catch (\GuzzleHttp\Exception\RequestException $e) { $this->app()->logException($e, false, 'Akismet HTTP error: '); }
			catch (\InvalidArgumentException $e) { $this->app()->logException($e, false, 'Akismet service error: '); }
		}
	}

	protected function _submitHam($params)
	{
		if (empty($params['akismetIsSpam']))
		{
			return; // Akismet already told us this wasn't spam, so do not submit as ham.
		}

		if (!empty($params['akismet']))
		{
			try
			{
				$params = [
					'headers' => [
						'User-Agent' => $this->getUserAgent()
					],
					'form_params' => $params['akismet'],
				];

				$response = $this->getHttpClient()->post('http://' . $this->getApiKey() . '.rest.akismet.com/1.1/submit-ham', $params);
				$response = trim($response->getBody()->getContents());

				if ($response == 'invalid')
				{
					throw new \InvalidArgumentException("Invalid API key");
				}
			}
			catch (\GuzzleHttp\Exception\RequestException $e) { $this->app()->logException($e, false, 'Akismet HTTP error: '); }
			catch (\InvalidArgumentException $e) { $this->app()->logException($e, false, 'Akismet service error: '); }
		}
	}

	protected function getParams(\XF\Entity\User $user, $message, $extraParams)
	{
		$params = parent::getParams($user, $message, $extraParams);

		if (isset($extraParams['is_test']))
		{
			$params['is_test'] = $extraParams['is_test'];
		}

		return $params;
	}

	protected function isSpam(\XF\Entity\User $user, $message, $extraParams)
	{
		$params = [
			'headers' => [
				'User-Agent' => $this->getUserAgent()
			],
			'form_params' => $this->getParams($user, $message, $extraParams),
		];

		$response = $this->getHttpClient()->post('http://' . $this->getApiKey() . '.rest.akismet.com/1.1/comment-check', $params);
		$response = trim($response->getBody()->getContents());

		if ($response == 'invalid')
		{
			throw new \InvalidArgumentException("Invalid API key");
		}

		return ($response == 'true');
	}

	/**
	 * @return \GuzzleHttp\Psr7\Request
	 */
	protected function getHttpRequest($url = null, $method = 'POST')
	{
		$client = $this->getHttpClient();
		$request = new Request($method, $url, [
			'headers' => [
				'User-Agent' => $this->getUserAgent()
			]
		]);

		return $request;
	}
}

