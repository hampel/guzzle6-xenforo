<?php namespace Guzzle6\XF\Spam\Checker;

class StopForumSpam extends XFCP_StopForumSpam
{
	protected function getSfsApiResponse(&$apiUrl = '', &$fromCache = false)
	{
		$apiUrl = $this->getSfsApiUrl();
		$cacheKey = $this->getSfsCacheKey($apiUrl);

		/** @var \XF\Spam\UserChecker $checker */
		$checker = $this->checker;

		if ($result = $checker->getRegistrationResultFromCache($cacheKey))
		{
			$fromCache = true;
			return unserialize($result);
		}

		$client = $this->app->http()->client();

		try
		{
			$response = $client->get($apiUrl);
			// Guzzle6 does not support json method on response objects
			$body = \GuzzleHttp\json_decode($response->getBody()->getContents(), true);

			return $body;
		}
		catch (\GuzzleHttp\Exception\RequestException $e)
		{
			return false;
		}
	}
}
