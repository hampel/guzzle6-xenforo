<?php

namespace Guzzle6\XF\Admin\Controller;

use Guzzle6\Test\AbstractTest;

class Tools extends XFCP_Tools
{
	public function actionTestGuzzle6()
	{
		$this->setSectionContext('testGuzzle6');

		$messages = [];
		$results = false;
		$test = '';

		if ($this->isPost())
		{
			$test = $this->filter('test', 'str');

			/** @var AbstractTest $tester */
			$tester = $this->app->container()->create('guzzle6.test', $test, [$this]);
			if ($tester)
			{
				$results = $tester->run();
				$messages = $tester->getMessages();
			}
			else
			{
				return $this->error(\XF::phrase('guzzle6_this_test_could_not_be_run'), 500);
			}
		}

		$viewParams = compact('results', 'messages', 'test');
		return $this->view('XF:Tools\TestGuzzle6', 'guzzle6_tools_test_guzzle6', $viewParams);
	}
}