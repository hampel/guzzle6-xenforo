Guzzle 6 implementation for XenForo 2.0
=======================================

Due to PHP version constraints, XenForo 2.0 implements Guzzle 5.3 rather than the current Guzzle 6.x

This XenForo 2.0 addon replaces the built in libraries with Guzzle 6.x and provides a test suite to ensure functionality
is preserved.

By [Simon Hampel](https://www.linkedin.com/in/hampel/).

Requirements
------------

This addon requires PHP 5.5 or higher and only works on XenForo 2.0.x 

The current release has been tested with the following versions of XenForo software and may require modifications to 
work with later releases:

* XenForo 2.0.5
* XenForo Media Gallery 2.0.4a
* XenForo Enhanced Search 2.0.1

Installation
------------

TODO: write installation instructions
