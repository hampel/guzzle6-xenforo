CHANGELOG
=========

1.2.1 (2018-05-09)
------------------

* update for XF 2.0.3 - no longer need to modify XF\BbCode\ProcessorAction\AutoLink::fetchUrlHtml - but instead need to 
  update new XF\Http\Reader::getCharset function
* update for XF 2.0.3 - new code in XF\Payment\Stripe
* update for XF 2.0.5 - no longer need to adjust Stripe class
* update for XFMG 2.0.3 - no longer need to extend functionality of XFMG\EmbedData\BaseData\getTitleAndDescription
* update for XFMG 2.0.3 - no longer need to extend functionality of XFMG\EmbedData\YouTube\getTitleAndDescription 
  or XFMG\EmbedData\Imgur\getTitleAndDescription 

1.2.0 (2018-02-03)
------------------

* Support for Media Gallery using Guzzle 6
* Added tests for Media Gallery

1.1.1 (2018-02-02)
------------------

* updates for XF 2.0.2 - XF\Http\Reader ... disable new initListeners functionality since Guzzle6 uses middleware 
  instead of listeners

1.1.0 (2018-01-15)
------------------

* added support for XenForo Elastic Search addon

1.0.6 (2018-01-13)
------------------

* bug fix: typo in if statement
* move to develop/release branches - no _output in release and no _data in develop

1.0.5 (2018-01-11)
------------------

* vendor/composer/autoload_files.php is only created when required, so don't log an error if the file doesn't exist
* use build.json to automatically run composer install with --no-dev and --optimize-autoloader flags set to get us a 
  clean build of dependencies for production deployment

1.0.4 (2017-12-29)
------------------

* bugfix: need to use XF\ConnectedAccount\Http\HttpResponseException

1.0.3 (2017-12-29)
------------------

* bugfix: still set form_params even when we have an empty array for the body

1.0.2 (2017-12-23)
------------------

* updates for XF 2.0.1 - XF\Service\AddOn\JsMinifier::request
* updates for XF 2.0.1 - XF\Service\ImageProxy::fetchImageDataFromUrl
* fixes for XF\ConnectedAccount\Http\Client::retrieveResponse - Sending application/x-www-form-urlencoded POST 
  requests requires that you specify the POST fields as an array in the form_params request options (also PUT)
* test routine for Connected Account Client
* change to using \GuzzleHttp\json_decode wrapper rather than native function

1.0.1 (2017-12-14)
------------------

* bugfix: missing use statements in Guzzle6\XF\Payment\PayPal; Guzzle6\XF\Payment\Stripe and 
  Guzzle6\XF\Payment\TwoCheckout

1.0.0 (2017-12-11)
------------------

* updates for XF 2.0.0 RC2 - changes to XF\SubContainer\Http
* updates for XF 2.0.0 - changes to XF\Service\ImageProxy::fetchImageDataFromUrl()
* Composer classloader changes (from Composer update)

0.1.9 (2017-11-08)
------------------

* update for XF 2.0.0 RC1 - changes to XF\Payment\Stripe::validateCallback

0.1.8 (2017-10-05)
------------------

* update for XF 2.0.0 Beta 5 - mirror changes to XF\Service\ImageProxy::fetchImageDataFromUrl

0.1.7 (2017-09-22)
------------------

* update for XF 2.0.0 Beta 3 - fixes for 2.0 beta 3 - now uses captchaIsValid() rather than captcha()->isValid()
* added _data and _releases to .gitignore so they aren't included in repo 

0.1.6 (2017-09-09)
------------------

* update for XF 2.0.0 Beta 1 - mirror changes to XF\Service\ImageProxy::fetchImageDataFromUrl

0.1.5 (2017-08-11)
------------------

* converted from hard-coded URL for link to options, to using buildLink and finder for OptionGroup
* Guzzle6 deprecates passing the body request option as an array to send a POST request 
* Guzzle6 does not support json method on response objects
* ReCaptcha; TextCaptcha; Akismet; StopForumSpam; JsMinifier; PayPal; Stripe; TwoCheckout
* Added Captcha Test
* Added Akismet Test
* Added StopForumSpam Test

0.1.4 (2017-08-03)
------------------

* update for DP10 - admin navigation entry changes
* updated to latest version of Guzzle - 6.3.0

0.1.3 (2017-06-13)
------------------

* removed .gitignore so _output directory now included in source control for multi-server or multi-user development 
  environments
* updated addon.json for DB8 with new defaults
* update for DP8: XF\BbCode\ProcessorAction\AutoLink previously replaced getUrlTitle, but the magic now happens in 
  fetchUrlHtml
* better description for code event listener

0.1.2 (2017-06-03)
------------------

* added extra information to error for autolinktest when "Convert URLs to page titles" option is not set

0.1.1 (2017-06-03)
------------------

* updated documentation
* added more details to addon.json

0.1.0 (2017-06-01)
------------------

* added PHP version dependencies to addon.json
* added test suite to ensure functionality is preserved
* initial working version
