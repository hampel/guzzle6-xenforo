<?php namespace Guzzle6\XFES\Elasticsearch;

use GuzzleHttp\Psr7\Request;
use XFES\Elasticsearch\Response;
use XFES\Elasticsearch\ConnectException;
use \GuzzleHttp\Exception\ConnectException as GuzzleConnectException;

class Api extends XFCP_Api
{
	public function __construct(array $config)
	{
		$this->config = array_replace($this->config, $config);

		if (!$this->config['index'])
		{
			$xfConfig = \XF::app()->config();
			if (isset($xfConfig['db']['dbname']))
			{
				$this->config['index'] = strtolower($xfConfig['db']['dbname']);
			}
		}

		if (!empty($this->config['singleType']))
		{
			// if we know it's single type, then trust that; otherwise, we'll still check the version
			$this->isSingleType = true;
		}

		$this->client = \XF::app()->http()->createClient([
			'base_uri' => $this->getBaseUrl(), // Guzzle 6 uses base_uri rather than base_url
			'timeout' => 20, // Guzzle 6 lets you set any number of default request options directly
			'connect_timeout' => 3, // Guzzle 6 lets you set any number of default request options directly
			'http_errors' => false // Guzzle 6 uses http_errors rather than exceptions
		]);
	}

	public function version()
	{
		if ($this->version === null)
		{
			$result = $this->request('get', '/')->getBody();
			if ($result && !empty($result['version']['number']))
			{
				$this->version = $result['version']['number'];
			}
			else
			{
				$this->version = 0;
				// Guzzle6\XFES\Elasticsearch\RequestException
				throw new RequestException("Could not find version number in request");
			}
		}

		return $this->version;
	}

	public function delete($type, $id)
	{
		try
		{
			return $this->requestById('delete', $type, $id)->getBody();
		}
		catch (RequestException $e) // Guzzle6\XFES\Elasticsearch\RequestException
		{
			$response = $e->getResponse();
			if ($response && $response->getCode() == 404)
			{
				// deleting something that has already been deleted shouldn't error
				return $response->getBody();
			}

			throw $e;
		}
	}

	public function indexExists()
	{
		if ($this->exists !== null)
		{
			return $this->exists;
		}

		try
		{
			$response = $this->request('head', $this->config['index']);
			$this->exists = ($response->getCode() == 200);
		}
		catch (RequestException $e) // Guzzle6\XFES\Elasticsearch\RequestException
		{
			// probably a "no such index" error
			$this->exists = false;
		}

		return $this->exists;
	}

	public function request($method, $path, $data = null, array $options = [], &$request = null)
	{
		// Guzzle 6 request changes
		try
		{
			if ($data)
			{
				if (!is_string($data))
				{
					$request = new Request($method, $path);
					$response = $this->client->send($request, [
						'json' => $data
					]);
				}
				else
				{
					$contentType = !empty($options['contentType']) ? $options['contentType'] : 'application/json';
					$headers = ['Content-Type' => $contentType];

					$request = new Request($method, $path, $headers, $data);
					$response = $this->client->send($request);
				}
			}
			else
			{
				$request = new Request($method, $path);
				$response = $this->client->send($request);
			}
			// end of Guzzle 6 request changes

			$body = $response->getBody();
			if ($body)
			{
				$contents = @json_decode($body->getContents(), true);
				$result = is_array($contents) ? $contents : [];
			}
			else
			{
				$result = null;
			}

			$esResponse = new Response($response->getStatusCode(), $result);
		}
		catch (GuzzleConnectException $e)
		{
			throw new ConnectException($e->getMessage(), $e->getCode(), $e);
		}

		$responseCode = $esResponse->getCode();
		if ($responseCode >= 400 && $responseCode <= 599)
		{
			$body = $esResponse->getBody();
			$error = null;

			if ($body)
			{
				$error = $this->getErrorMessage($body);
			}

			if (!$error)
			{
				$error = "Unknown error (HTTP code: $responseCode)";
			}

			/** @var RequestException $e */
			$e = new RequestException($error, $responseCode);
			$e->setRequest($request);
			$e->setResponse($esResponse);

			throw $e;
		}

		return $esResponse;
	}

	public function bulkRequest($data)
	{
		$options = [
			'contentType' => 'application/x-ndjson'
		];

		$esResponse = $this->request('post', '_bulk', $data, $options, $request);
		$body = $esResponse->getBody();

		if ($this->hasBulkActionErrors($body, $errors))
		{
			$e = new BulkRequestException($errors);
			$e->setRequest($request);
			$e->setResponse($esResponse);

			throw $e;
		}

		return $esResponse;
	}
}
