<?php namespace Guzzle6\XFES\Elasticsearch;

// this class replaces XFES\Elasticsearch\BulkRequestException because it extends
// Guzzle6\XFES\Elasticsearch\RequestException which replaces XFES\Elasticsearch\RequestException because we needed to
// change the signature of one of the methods

class BulkRequestException extends RequestException
{
	protected $bulkErrors = [];

	public function __construct($errors, $code = 0, \Exception $previous = null)
	{
		if (is_string($errors))
		{
			$errors = [$errors];
		}
		$this->bulkErrors = $errors;

		$firstError = 'Unknown error';
		if (is_array($errors) && count($errors) > 0)
		{
			$key = key($errors);
			$firstError = "[$key] " . reset($errors);
		}

		parent::__construct("Elasticsearch bulk action error (first error: $firstError)", $code, $previous);
	}

	public function getBulkErrors()
	{
		return $this->bulkErrors;
	}
}
