<?php namespace Guzzle6\XFES\Elasticsearch;

use XFES\Elasticsearch\Response;
use XFES\Elasticsearch\Exception;
use Psr\Http\Message\RequestInterface; // Guzzle 6 uses PRS7 requests

// this class replaces XFES\Elasticsearch\RequestException because we needed to change the signature of the
// setRequest method

class RequestException extends Exception
{
	/** @var RequestInterface */
	protected $request;

	/** @var Response */
	protected $response;

	// Guzzle 6 uses PRS7 requests
	public function setRequest(RequestInterface $request)
	{
		$this->request = $request;
	}

	public function getRequest()
	{
		return $this->request;
	}

	public function setResponse(Response $response)
	{
		$this->response = $response;
	}

	public function getResponse()
	{
		return $this->response;
	}
}
