<?php namespace Guzzle6\XFES\Service;

use XFES\Elasticsearch\Exception;
use Guzzle6\XFES\Elasticsearch\RequestException;

class Optimizer extends XFCP_Optimizer
{
	public function isOptimizable()
	{
		try
		{
			$config = $this->es->getIndexInfo();
		}
		catch (RequestException $e)
		{
			return true;
		}
		catch (Exception $e)
		{
			return false;
		}

		if (!$config['mappings'])
		{
			return true;
		}

		$liveMappings = $config['mappings'];
		$mappingType = key($liveMappings);
		if ($mappingType == '_doc')
		{
			// ES7 may not have an explicit type, so we may _doc as the type, whereas we're expecting "xf".
			// When this happens, we won't be passing types into URLs, so we should be able to ignore it.
			$liveMappings = [$this->es->getSingleTypeName() => $liveMappings['_doc']];
		}

		$expectedMappings = $this->getExpectedMappingConfig();

		return ($liveMappings != $expectedMappings);
	}
}
